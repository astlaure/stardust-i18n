# Stardust I18n Wordpress Plugin

## Features

1. Provide multilanguage support for Post, Pages, Menus, Sidebars and Custom Posts
2. The default route has no prefix and the others have /{language} prefixes

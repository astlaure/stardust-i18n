<?php

if ( !class_exists( 'StardustI18nPermalink' ) ) {
    class StardustI18nPermalink {
        function __construct() {
            add_filter( 'post_link', array( $this, 'localize_permalink' ), 10, 2 );
            add_filter( 'page_link', array( $this, 'localize_permalink' ), 10, 2 );
            add_filter( 'category_link', array( $this, 'localize_permalink' ), 10, 2 );
            add_filter( 'author_link', array( $this, 'localize_permalink' ), 10, 2 );
        }

        public function localize_permalink( $url, $post ){
            global $star_language;
            global $star_default_language;

            $path = str_replace(site_url(), '', $url);

            if ($star_language != $star_default_language) {
                return site_url() . '/' . $star_language . $path;
            }

            return site_url() . $path;
        }
    }
}

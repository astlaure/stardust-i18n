<?php

if ( !class_exists( 'StardustI18nUrlRewrite' ) ) {
    class StardustI18nUrlRewrite {
        function __construct() {
            add_action( 'plugins_loaded', array( $this, 'localization_uri_process' ) );
        }

        public function localization_uri_process() {
            global $star_language;
            global $star_default_language;
            global $star_supported_languages;

            $sections = explode('/', $_SERVER['REQUEST_URI']);

            if (sizeof($sections) > 1 && in_array($sections[1], $star_supported_languages)) {
                $star_language = $sections[1];
                $_SERVER['REQUEST_URI'] = str_replace('/' . $sections[1], '', $_SERVER['REQUEST_URI']);

                if ( substr($_SERVER['REQUEST_URI'], -1) !== '/' ) {
                    $_SERVER['REQUEST_URI'] .= '/';
                }
            } else {
                $star_language = $star_default_language; // your default language
            }
        }
    }
}

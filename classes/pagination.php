<?php

if ( !class_exists( 'StardustI18nPagination' ) ) {
    class StardustI18nPagination {
        function __construct() {
            add_filter( 'get_next_post_join', array( $this, 'get_post_join' ), 10 );
            add_filter( 'get_next_post_where', array( $this, 'get_post_where' ), 10 );
            add_filter( 'get_previous_post_join', array( $this, 'get_post_join' ), 10 );
            add_filter( 'get_previous_post_where', array( $this, 'get_post_where' ), 10 );
        }

        public function get_post_join( $join ) {
            global $post;
            if ( get_post_type( $post ) == 'post' ) {
                global $wpdb;
                return $join . "INNER JOIN $wpdb->postmeta AS m ON p.ID = m.post_id ";
            }
            return $join;
        }

        public function get_post_where( $where ) {
            global $post;
            global $star_language;
            if ( get_post_type( $post ) == 'post' ) :
//				global $wpdb;
//				$where = $wpdb->prepare("WHERE p.post_title {$operator} '%s' AND p.post_type = 'work' AND p.post_status = 'publish' AND (m.meta_key = 'not_clickable' AND (m.meta_key = 'not_clickable' AND m.meta_value != 1))", $post->post_title );
                return $where . " AND (m.meta_key = 'language' AND m.meta_value = '$star_language')";
            endif;

            return $where;
        }
    }
}

import { compose } from '@wordpress/compose';
import { PanelRow, SelectControl } from '@wordpress/components';
import { withDispatch, withSelect } from "@wordpress/data";
import { VALIDATOR_LABEL } from '../validator';

const screens = ['post', 'page'];

const LanguageField = ({ postType, postMeta, setPostMeta, validate }) => {
    // Initial setup
    // if ( !screens.includes(postType) ) return null;

    if (!window[VALIDATOR_LABEL]['language']) {
        validate(postMeta);
    }

    return (
        <PanelRow style={{display: 'block'}}>
            <div style={{width: '100%', padding: '1px 0'}}>
                <SelectControl
                    label="Language"
                    value={ postMeta.language }
                    onChange={ ( value ) => setPostMeta({...postMeta, language: value}) }
                    onBlur={() => validate(postMeta)}
                    options={[
                        { label: '--', value: '', disabled: true },
                        { label: 'Francais', value: 'fr' },
                        { label: 'English', value: 'en' },
                    ]}
                />
            </div>
        </PanelRow>
    )
};

export default compose( [
    withSelect( ( select ) => {
        return {
            postMeta: select( 'core/editor' ).getEditedPostAttribute( 'meta' ),
            postType: select( 'core/editor' ).getCurrentPostType(),
            posts: select( 'core' ).getEntityRecords( 'postType', 'post' ),
        };
    } ),
    withDispatch( ( dispatch ) => {
        return {
            setPostMeta( newMeta ) {
                dispatch( 'core/editor' ).editPost( { meta: newMeta } );
            },
            validate( meta ) {
                window[VALIDATOR_LABEL]['language'] = true;
                if (meta.language === '') {
                    dispatch( 'core/notices' ).createNotice( 'warning', 'Language is not set!', {
                        id: 'meta-language',
                        isDismissible: false,
                    } );
                    dispatch( 'core/editor' ).lockPostSaving( 'meta-language' );
                } else {
                    dispatch( 'core/editor' ).unlockPostSaving( 'meta-language' );
                    dispatch( 'core/notices' ).removeNotice('meta-language');
                }
            },
            lockSave() {
                console.log('lock');
                dispatch( 'core/notices' ).createErrorNotice( 'Something is not good!' );
                dispatch( 'core/editor' ).lockPostSaving( 'meta-language' );
            }
        };
    } )
] )( LanguageField );

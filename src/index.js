import './validator';
import { registerPlugin } from "@wordpress/plugins";
import StardustPanel from './stardust-panel';

registerPlugin( 'stardust-i18n', {
  render: StardustPanel,
} );

import { PluginDocumentSettingPanel } from '@wordpress/edit-post';
import { compose } from '@wordpress/compose';
import { withSelect } from '@wordpress/data';
import LanguageField from '../language-field';

const screens = ['post', 'page'];

const StardustPanel = ({ postType }) => {
  // if ( !screens.includes(postType) ) { return null; }

  return (
    <PluginDocumentSettingPanel title="Stardust I18n" initialOpen="true">
      <LanguageField />
    </PluginDocumentSettingPanel>
  )
};

export default compose([
  withSelect( ( select ) => {
    return {
      postType: select( 'core/editor' ).getCurrentPostType(),
    };
  }),
])(StardustPanel);

<?php
/*
Plugin Name: Stardust I18n
Description: I18n Plugin for the Stardust Ecosystem.
Version: 1.0.0
Author Name: Alexandre St-Laurent
Author URI: https://github.com/astlaure
*/

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'StardustI18n' ) ) {
    class StardustI18n {
        static $screens = array( 'post', 'page' );

        function __construct() {
            add_action( 'init', array( $this, 'register_language' ) );
            add_filter( 'pre_get_posts', array( $this, 'pre_get_posts' ) );

            add_action( 'init', array( $this, 'gutenberg_register_scripts' ) );
            add_action( 'enqueue_block_editor_assets', array( $this, 'gutenberg_enqueue_scripts' ) );

            require_once __DIR__ . '/classes/url-rewrite.php';
            require_once __DIR__ . '/classes/permalink.php';
            require_once __DIR__ . '/classes/pagination.php';

            new StardustI18nUrlRewrite();
            new StardustI18nPermalink();
            new StardustI18nPagination();
        }

        public function register_language() {
            global $star_custom_posts;

            $screens = array_merge($this::$screens, $star_custom_posts);
            foreach ( $screens as $screen ) {
                register_post_meta($screen, 'language', array(
                    'type' => 'string',
                    'single' => true,
                    'show_in_rest' => true,
                ));
            }
        }

        /**
         * The main query of the /en/ page tries to load the accueil page
         * @param $query
         */
        public function pre_get_posts( $query ) {
            global $star_language;

            if ( ! is_admin() && !is_page() && $query->is_main_query() ) { // TODO INVESTIGATE need is_page or else it does too many redirect on /en/ if the page has the language set
                $query->set( 'meta_query', array(
                    'relation' => 'OR',
                    array(
                        'key' => 'language',
                        'value' => $star_language,
                        'compare' => '='
                    ),
                    array(
                        'key' => 'language',
                        'compare' => 'NOT EXISTS'
                    )
                ) );
            }
        }

        public function gutenberg_register_scripts() {
            $asset_file = include( plugin_dir_path( __FILE__ ) . 'build/index.asset.php');

            wp_register_script(
                'stardust-i18n-editor',
                plugins_url( 'build/index.js', __FILE__ ),
                $asset_file['dependencies'],
                $asset_file['version']
            );
        }

        public function gutenberg_enqueue_scripts() {
            if ( get_current_screen()->id === 'widgets' ) {
                return;
            }

            wp_enqueue_script( 'stardust-i18n-editor' );
        }

        static public function activate() {
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

            if ( !is_plugin_active( 'stardust-core/stardust-core.php' ) ) {
                ?>
                <div class="notice notice-error" >
                    <p>Please enable Stardust Core before activating Stardust I18n</p>
                </div>
                <?php
                @trigger_error(__( 'Please enable Stardust Core before activating Stardust I18n', 'stardust-i18n' ), E_USER_ERROR);                
            }

            update_option( 'rewrite_rules', '' );
        }

        static public function deactivate() {
            flush_rewrite_rules();
        }
        
        static public function uninstall() {}
    }
}

if ( class_exists( 'StardustI18n' ) ) {
    register_activation_hook( __FILE__, array( 'StardustI18n', 'activate' ) );
	register_deactivation_hook( __FILE__, array( 'StardustI18n', 'deactivate' ) );
	register_uninstall_hook( __FILE__, array( 'StardustI18n', 'uninstall' ) );

    new StardustI18n();
}
